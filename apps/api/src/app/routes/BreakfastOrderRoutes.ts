import * as express from 'express';
import {Request, Response} from "express";
import * as breakfastOrderModel from "../models/BreakfastOrderf";
import {BreakfastOrder, BreakfastOrderAnzeige} from "@kvse/api-interfaces";

export const breakfastOrderRoutes = express.Router();

breakfastOrderRoutes.get("/:id", async (req: Request, res: Response) => {
  const orderId = Number(req.params.id);
  breakfastOrderModel.findOne(orderId, (err: Error, orders: BreakfastOrderAnzeige[]) => {
    if (err) {
      return res.status(500).json({"errorMessage": err.message});
    }
    res.status(200).json({"data": orders});
  });
});

breakfastOrderRoutes.delete("/:id", async (req: Request, res: Response) => {
  const orderId = Number(req.params.id);
  breakfastOrderModel.deleteOne(orderId, (err: Error, id: number) => {
    if (err) {
      return res.status(500).json({"errorMessage": err.message});
    }
    res.status(200).json({"data": id });
  });
});

breakfastOrderRoutes.post("/", async (req: Request, res: Response) => {
  const order: BreakfastOrderAnzeige = req.body
  breakfastOrderModel.create(order, (err: Error, id: number) => {
    if (err) {
      return res.status(500).json({"errorMessage": err.message});
    }
    res.status(200).json({"data": "ok"})
  });
});
