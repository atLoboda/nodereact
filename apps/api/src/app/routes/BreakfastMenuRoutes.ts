import * as express from 'express';
import {Request, Response} from "express";
import * as breakfastMenuModel from "../models/BreakfastMenu";
import {BreakfastMenu} from "@kvse/api-interfaces";

export const breakfastMenuRoutes = express.Router();

breakfastMenuRoutes.get("/", async (req: Request, res: Response) => {
  breakfastMenuModel.findAll((err: Error, menus: BreakfastMenu[]) => {
    if (err) {
      return res.status(500).json({"errorMessage": err.message});
    }
    res.status(200).json(menus);
  });
});
