import * as express from 'express';
import {Request, Response} from "express";
import * as breakfastOrderModel from "../models/BreakfastOrderf";
import {BreakfastOrder, BreakfastOrderAnzeige} from "@kvse/api-interfaces";

export const breakfastOrdersRoutes = express.Router();

breakfastOrdersRoutes.get("/", async (req: Request, res: Response) => {
  breakfastOrderModel.findAll((err: Error, orders: BreakfastOrderAnzeige[]) => {
    if (err) {
      return res.status(500).json({"errorMessage": err.message});
    }
    res.status(200).json(orders);
  });
});
