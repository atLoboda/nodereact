import {BreakfastMenu, BreakfastMenuPOS, BreakfastOrder, BreakfastOrderAnzeige, Kunde} from "@kvse/api-interfaces";
import {db} from "../db";
import {OkPacket, RowDataPacket} from "mysql2"
import {forEachExecutorOptions} from "@nrwl/workspace/src/utilities/executor-options-utils";
import {insert} from "@nrwl/workspace";


export const findAll = (callback: CallableFunction) => {
  const queryString = "select K.zimmernummer, K.name, b.id, b.lieferzeit, G.bezeichnung, BG.anzahl, G.preis\n" +
    "                from Bestellung b\n" +
    "                join Bestellung_Gerichte BG on b.id = BG.bestellung_id\n" +
    "                join Kunde K on K.id = b.id_kunde\n" +
    "                join Gerichte G on BG.gerichte_id = G.id order by b.id desc"

  db.query(queryString, (err, result) => {
    if (err) {
      callback(err)
    }
    const rows = <RowDataPacket[]>result;
    const breakfastOrders: BreakfastOrderAnzeige[] = [];

    rows.forEach(row => {
      const foundOrder: BreakfastOrderAnzeige = breakfastOrders.find(x => x.id == row.id);

      const pos: BreakfastMenuPOS = {
        name: row.bezeichnung,
        anzahl: row.anzahl
      }

      if (foundOrder === undefined) {
        const order: BreakfastOrderAnzeige = {
          id: row.id,
          zimmernummer: row.zimmernummer,
          name: row.name,
          lieferzeit: row.lieferzeit,
          positionen: [],
          gesamtpreis: 0
        }
        order.positionen.push(pos)
        order.gesamtpreis = pos.anzahl * row.preis
        breakfastOrders.push(order)
      } else {
        foundOrder.positionen.push(pos)
        foundOrder.gesamtpreis += pos.anzahl * row.preis
      }
    })
    callback(null, breakfastOrders)
  })
}

export const findOne = (orderId: number, callback: Function) => {
  const queryString = "select K.zimmernummer, K.name, b.id, b.lieferzeit, G.bezeichnung, BG.anzahl, G.preis\n" +
    "                from Bestellung b\n" +
    "                join Bestellung_Gerichte BG on b.id = BG.bestellung_id\n" +
    "                join Kunde K on K.id = b.id_kunde\n" +
    "                join Gerichte G on BG.gerichte_id = G.id" +
    " where b.id = ? order by b.id desc"

  db.query(queryString, orderId, (err, result) => {
    if (err) {
      callback(err)
    }

    const rows = <RowDataPacket[]>result;
    const breakfastOrders: BreakfastOrderAnzeige[] = [];

    rows.forEach(row => {
      const foundOrder: BreakfastOrderAnzeige = breakfastOrders.find(x => x.id == row.id);

      const pos: BreakfastMenuPOS = {
        name: row.bezeichnung,
        anzahl: row.anzahl
      }

      if (foundOrder === undefined) {
        const order: BreakfastOrderAnzeige = {
          id: row.id,
          zimmernummer: row.zimmernummer,
          name: row.name,
          lieferzeit: row.lieferzeit,
          positionen: [],
          gesamtpreis: 0
        }
        order.positionen.push(pos)
        order.gesamtpreis = pos.anzahl * row.preis
        breakfastOrders.push(order)
      } else {
        foundOrder.positionen.push(pos)
        foundOrder.gesamtpreis += pos.anzahl * row.preis
      }
    })
    callback(null, breakfastOrders)
  })
}

export const deleteOne = (orderId: number, callback: Function) => {
  let queryString = "DELETE FROM Bestellung_Gerichte WHERE bestellung_id = ?"

  db.query(queryString, orderId,(err, result) => {
    if (err) {
      callback(err)
    }
    console.log('Deleted Row(s):', result);
  })

  queryString = "DELETE FROM Bestellung WHERE id = ?"
  db.query(queryString, orderId,(err, result) => {
    if (err) {
      callback(err)
    }
    console.log('Deleted Row(s):', );

    const affectedRows  = (<OkPacket> result).affectedRows;
    callback(null, affectedRows)
  })
}


export const create = (order : BreakfastOrderAnzeige, callback: Function) => {

  let queryString = "SELECT * FROM Kunde WHERE name = ? and zimmernummer = ?"
  const kunde : Kunde = {};
  db.query(queryString,[order.name, order.zimmernummer],(err,result) => {

    if(err){
      callback(err)
    }

    const rows = <RowDataPacket[]>result;
    kunde.id = rows[0].id;
    kunde.name = rows[0].name;
    kunde.zimmernummer = rows[0].zimmernumrmer;


    let insertID;
    queryString = "INSERT INTO Bestellung (id, id_kunde, lieferzeit) VALUES (default,?,?)"
    db.query(queryString,[kunde.id,order.lieferzeit],(err,result) => {
      if(err){
        callback(err)
      }
      insertID  = (<OkPacket> result).insertId;
      console.log("Bestellung insert: " + insertID)

      order.positionen.forEach(x => {
        queryString = "INSERT INTO Bestellung_Gerichte (id, bestellung_id, gerichte_id, anzahl) VALUES (default, ?,?,?)"
        db.query(queryString,[insertID,x.gerichteID,x.anzahl], (err,result) => {
          if(err){
            callback(err)
          }
          insertID  = (<OkPacket> result).insertId;
          console.log("Bestellung_Gerichte insert: " + insertID)
        })
      })
      callback(null, insertID)
    })
  })
}


