import {BreakfastMenu} from "@kvse/api-interfaces";
import {db} from "../db";
import {OkPacket, RowDataPacket} from "mysql2"

export const findAll = (callback: CallableFunction) => {
  const queryString = "SELECT * from Gerichte"

  db.query(queryString, (err, result) => {
    if (err) {
      callback(err)
    }
    const rows = <RowDataPacket[]>result;
    const breakfastMenus: BreakfastMenu[] = [];

    rows.forEach(row => {
      const breakfastMenu: BreakfastMenu = {
        id: row.id,
        bezeichnung: row.bezeichnung,
        preis: row.preis
      }
      breakfastMenus.push(breakfastMenu);
    })
    callback(null, breakfastMenus)
  })

}
