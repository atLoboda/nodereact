import * as express from 'express';
import * as bodyParser from "body-parser";
import {breakfastMenuRoutes} from "./app/routes/BreakfastMenuRoutes";
import expressOasGenerator = require('express-oas-generator')
import {breakfastOrderRoutes} from "./app/routes/BreakfastOrderRoutes";
import {breakfastOrdersRoutes} from "./app/routes/BreakfastOrdersRoutes";
import cors = require('cors');
const app = express();

app.use(cors());
// Swagger generieren
expressOasGenerator.init(app,{})

app.use(bodyParser.json())
app.use("/menus", breakfastMenuRoutes);
app.use("/orders", breakfastOrdersRoutes);
app.use("/order", breakfastOrderRoutes);

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log('Listening at http://localhost:' + port + '/api');
});
server.on('error', console.error);
