import React, {useEffect, useState} from 'react';
import {BreakfastOrderAnzeige, Message} from '@kvse/api-interfaces';
import OrderList from "./components/OrderList";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Order from "./components/Order";
import Error from "./components/Error";


export const App = () => {
  return (
    <>
      <main>
        <Switch>
          <Route path='/' component={OrderList} exact />
          <Route path='/breakfastOrder' component={Order} exact/>
          <Route component={Error}/>
        </Switch>
      </main>
    </>
  );
};

export default App;
