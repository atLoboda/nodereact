import React, {useEffect, useState} from "react";
import {BreakfastMenu, BreakfastMenuPOS, BreakfastOrderAnzeige} from "@kvse/api-interfaces";


class Order extends React.Component {

  constructor(props) {
    super(props);

    const order: BreakfastOrderAnzeige = {}
    this.state = {
      zimmernummer: 0,
      name: "",
      lieferzeit: Date(),
      menus: [],
      order: order,
      positionen: []
    }

    this.getMenuData();
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleMenuInputChange = this.handleMenuInputChange.bind(this);
    this.createOrder = this.createOrder.bind(this)

  };

  public widthStyle = {
    width: 600
  }

  menu: BreakfastMenu[] = [];

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleMenuInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    let pos: BreakfastMenuPOS = {};
    pos.gerichteID = name;
    pos.anzahl = value

    let posOld: BreakfastMenuPOS = this.state.positionen.find(x => x.gerichteID == name)
    if (posOld !== undefined) {
      posOld.anzahl = value
    } else {
      this.state.positionen.push(pos);
    }
  }

  getMenuData() {
    const order: BreakfastOrderAnzeige = {}
    fetch('http://localhost:3333/menus')
      .then((r) => r.json())
      .then(items => {
        this.setState(({menus: items}));
      })
  }

  createOrder() {

    const order: BreakfastOrderAnzeige = {}
    order.positionen = this.state.positionen;
    order.name = this.state.name;
    order.zimmernummer = this.state.zimmernummer;
    order.lieferzeit = this.state.lieferzeit;

    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(order)
    };

    fetch('http://localhost:3333/order/', requestOptions)
      .then(response => response.json())
      .then(data => this.setState({postId: data.id}));
    this.abort()
  }

  abort() {
    window.location.href = ('/')
  }

  render() {
    return (
      <div className="wrapper" style={this.widthStyle}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <h2 className="mt-5">Bestellung erstellen</h2>
              <p>Bitte ausfüllen und bestätigen</p>
              <form>

                <div className="form-group">
                  <label>Zimmernummer</label>
                  <input type="number" name="zimmernummer" className="form-control" value={this.state.zimmernummer}
                         onChange={this.handleInputChange}
                  />
                </div>

                <div className="form-group">
                  <label>Name</label>
                  <input type="text" name="name" className="form-control" value={this.state.name}
                         onChange={this.handleInputChange}/>
                </div>


                <div className="form-group">
                  <label>Uhrzeit</label>
                  <input type="datetime-local" name="lieferzeit" value={this.state.lieferzeit}
                         onChange={this.handleInputChange} className="form-control"
                  />
                </div>
                <div className="form-group">
                  <label>Auswahl Frühstück</label>
                  <table className="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Bezeichnung</th>
                      <th>Preis</th>
                      <th>Anzahl</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.menus.map(item => {
                      return (
                        <tr>
                          <td>{item.bezeichnung}</td>
                          <td>{item.preis} €</td>
                          <td><input type="number" name={item.id} onChange={this.handleMenuInputChange}
                                     className="form-control"/></td>
                        </tr>
                      )
                    })}
                    </tbody>
                  </table>
                </div>

                <div className="mb-5"></div>
                <input onClick={this.createOrder} className="btn btn-primary" value="Bestellen"/>
                <a onClick={this.abort} className="btn btn-secondary ml-2">Abbrechen</a>
              </form>
              <div className="mb-5"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}


export default Order;
