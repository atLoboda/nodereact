import React, {useEffect, useState} from "react";
import {BreakfastOrderAnzeige} from "@kvse/api-interfaces";
import { Link } from "react-router-dom";

export const OrderList = () => {
  const [orders, setOrders] = useState<BreakfastOrderAnzeige[]>([]);

  useEffect(() => {
    fetch('http://localhost:3333/orders')
      .then((r) => r.json())
      .then(items => {
        setOrders(items);
      })
  }, []);

  function deleteRecord(id: number) {
    return function (p1: React.MouseEvent<HTMLAnchorElement>) {
      fetch('http://localhost:3333/order/' + id, {method: 'DELETE'})
        .then(() => console.log("delete successful"));
      window.location.reload();
    };
  }

  return (
    <>
      <div className="wrapper">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="mt-5 mb-3 clearfix">
                <h2 className="pull-left">Frühstücksbuchung</h2>
                <a href="/breakFastOrder" className="btn btn-success pull-right"><i className="fa fa-plus"></i> Neue
                  Bestellung</a>
              </div>

              <table className="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Zimmernummer</th>
                  <th>Name</th>
                  <th>Lieferzeitpunkt</th>
                  <th>Artikel</th>
                  <th>Gesamtpreis</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {orders.map(item => {
                  return (
                    <tr>
                      <td>{item.id}</td>
                      <td>{item.zimmernummer}</td>
                      <td>{item.name}</td>
                      <td>{item.lieferzeit}</td>
                      <td>{item.positionen.map(x => {
                        return (
                          x.name + "(" + (x.anzahl) + ") "
                        )
                      })}</td>
                      <td key={item.id}>{parseFloat(item.gesamtpreis).toFixed(2)} €</td>
                      <td><a onClick={deleteRecord(item.id)} href="#" title="Delete Record" data-toggle="tooltip"><span
                        className="fa fa-trash"></span></a></td>
                    </tr>
                  )
                })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div>
        <Link to="/">Home </Link>
        <Link to="/breakfastOrder">About Us </Link>
        <Link to="/shop">Shop Now </Link>
      </div>
    </>
  );
};

export default OrderList;
