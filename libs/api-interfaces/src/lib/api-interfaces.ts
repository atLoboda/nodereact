export interface Message {
  message: string;
}

export interface BreakfastMenu {
  id: number,
  bezeichnung: string,
  preis: number
}

export interface BreakfastMenuPOS {
  gerichteID?: number,
  name: string,
  anzahl: number
}

export interface BreakfastOrder {
  id: number,
  zimmernummer: number,
  name: string,
  lieferzeit: Date,
  preis: number,
  bezeichnung: string,
  anzahl: number
}

export interface BreakfastOrderAnzeige{
  id?: number,
  zimmernummer?: number,
  name?: string,
  lieferzeit?: Date,
  positionen?: BreakfastMenuPOS[]
  gesamtpreis?: number,
}

export interface Kunde {
  id?:number,
  zimmernummer?: number,
  name?: string
}
